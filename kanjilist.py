# -*- coding: utf-8 -*-

import os.path
import re
import itertools
import csv
import operator
#from charutils import isOnlyKatakana, isOnlyHiragana

class KanjiList:
    def __init__(self):
        self.jlptList = [[], [], [], [], []]
        self.joyoList = []
        self.kyouikuList = []

        fileName = os.path.join(os.path.dirname(__file__), 'jlpt_joyo_list.csv')
        with open(fileName, 'r', encoding='utf8') as csvfile:
            reader = csv.reader(csvfile)
            next(reader, None) # skip headers
            for row in reader:
                groups = list(row)
                if len(groups) != 7:
                  raise RuntimeError('Incorrect line format:' + str(row))
                joyoId, kanji, oldKanji, strokes, \
                grade, jlptLevel, meaning  = groups

                if len(grade) !=0 and grade.isdigit() and int(grade) <= 6:
                  self.kyouikuList.append(kanji)
                  assert(len(joyoId) != 0)

                if len(joyoId) != 0:
                    self.joyoList.append(kanji)

                if len(jlptLevel) != 0:
                    level = int(jlptLevel)
                    if level > 5:
                        raise RuntimeError('Incorrect JLPT level:' + jlptLevel + ", " + kanji)
                    for i in range(level):
                        self.jlptList[i].append(kanji)

           # if not set(self.kyouikuList).issubset(set(self.jlptLevel)):
           #     raise RuntimeError("Not all Kyouiku kanji in Joyo list")


    def getJoyoList(self):
        return self.joyoList

    def getKyouikuList(self):
        return self.kyouikuList

    def getJlptList(self, level):
        return self.jlptList[level]


def main():
    pass

if __name__ == '__main__':
    main()
