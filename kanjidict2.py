# -*- coding: utf-8 -*-

import os.path
import re
import itertools
import csv
from charutils import isOnlyKatakana, isOnlyHiragana

#rx = re.compile('^-?(\w+)(?:-|(?:\.(\w+))?)-?', re.S | re.U)
rx = re.compile(
"""
    ^(-?)         # succeeds another root: 支払い
    (\w+)         # main reading
    (?:\.(\w+))?  # reading from masu form of a verb 払う -> 払い -> 払
    (-?)          # preceeds another root: 上着
    (\w*)
"""
, re.S | re.U | re.X)

masuForm = { 'う'  : 'い', 'く'  : 'き', 'す': 'し', 'つ': 'ち',
             'ぬ' : 'ね', 'ぶ' : 'び', 'む': 'み', 'る': 'り' }

def splitReadings(readings):
    onyomi = set()
    kunyomi = set()
    masuKunyomi = set()
    for yomi in readings.split(' '):
        if isOnlyKatakana(yomi):
            onyomi.add(yomi)
            continue

        m = rx.match(yomi)
        if m:
            kunyomi.add(m.group(2))
            masuEnding = m.group(3)
            if masuEnding is not None:
                # assuming all ru-verbs are u-verbs
                ending = masuForm.get( masuEnding, masuEnding )
                masuKunyomi.add( m.group(2) + ending )
                if masuEnding[-1] == 'る':
                    masuKunyomi.add( m.group(2) + masuEnding[:-1] )
        else:
            raise RuntimeError('Mixing katakana/hiragana: ' + yomi)
    return onyomi, kunyomi, masuKunyomi

class Kanji:
   def __init__(self, id, kanji, readings, meaning):
      self.id      = id
      self.kanji   = kanji
      self.meaning = meaning
      self.onyomi, self.kunyomi, \
         self.masuKunyomi = splitReadings(readings)



class KanjiDict2:
    """Super Kanji dictionary 12K records"""
    def __init__(self):
        self.kanjiList = {}
        fileName = os.path.join(os.path.dirname(__file__), 'kanji.txt')
        with open(fileName, 'r', encoding='utf8') as f:
            id = 1
            for line in f.readlines():
              line = line.rstrip('\n')
              groups = line.split('|')
              if len(groups) != 6:
                  RuntimeError('Unknown format in line: ' + line)
              kanji, chineseInfo, japaneseInfo , \
              nanori, _, meaning = groups
              self.kanjiList[kanji] = Kanji(id, kanji, japaneseInfo, meaning)
              id += 1


    def getKanji(self, kanji):
        return self.kanjiList[kanji]


if __name__ == '__main__':
   pass