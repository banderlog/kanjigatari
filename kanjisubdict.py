﻿# -*- coding: utf-8 -*-

import operator
#from charutils import isOnlyKatakana, isOnlyHiragana

def createOnyomiMap(kanjiList):
    """ Creates map of Kun kanji reading
        kanjiList - list of Kanji objects
    """
    readings = {}
    for k in kanjiList:
        for yomi in k.onyomi:
            readings.setdefault(yomi, []).append(k.id)
    return readings

def createKunyomiMap(kanjiList):
    """ Creates map of Kun kanji reading
        kanjiList - list of Kanji objects
    """
    readings = {}
    for k in kanjiList:
        for yomi in k.kunyomi:
            readings.setdefault(yomi, []).append(k.id)
    return readings

class KanjiSubDict:
    """Extraction from the main dictionary basing on a kanji list"""
    def __init__(self, mainDictionary, kanjiList):
        """
            mainDictionary  KanjiDict2 object
            kanjiList       list of kanji symbols
        """
        #allKanji = dict( (k.kanji, k) for k in mainDictionary.kanjiList )
        self.kanjiList = []
        for k in kanjiList:
            self.kanjiList.append(mainDictionary.getKanji(k))

        self.onyomiMap = createOnyomiMap(self.kanjiList)
        self.kunyomiMap = createKunyomiMap(self.kanjiList)

        self.idOrder = list((item.id, item.kanji) for item in self.getList())

        self.onyomiOrder = []
        for k in self.kanjiList:
            minOn = ''
            if len(k.onyomi):
                minOn = min(k.onyomi)
            self.onyomiOrder.append((k.kanji, k.id, minOn))
        self.onyomiOrder = list(map( lambda x: (x[1], x[0]), sorted(self.onyomiOrder, key=operator.itemgetter(2)) ))

        self.kunyomiOrder = []
        for k in self.kanjiList:
            minKun = ''
            if len(k.kunyomi):
                minKun = min(k.kunyomi)
            self.kunyomiOrder.append((k.kanji, k.id, minKun))
        self.kunyomiOrder = list(map( lambda x: (x[1], x[0]), sorted(self.kunyomiOrder, key=operator.itemgetter(2)) ))

    def getList(self):
        return self.kanjiList

    def getAllTags(self):
        return set()

    def getTagMap(self):
        return []#set()

    def getOnyomiMap(self):
        return self.onyomiMap

    def getKunyomiMap(self):
        return self.kunyomiMap

    def getSortOrder(self, sortby):
        return sortby in ['id', 'onyomi', 'kunyomi']

    def getKanjiTable(self, sortby):
        if len(sortby) == 0 or sortby == 'id':
            return self.idOrder
        elif sortby == 'onyomi':
            return self.onyomiOrder
        elif sortby == 'kunyomi':
            return self.kunyomiOrder

def main():
    pass

if __name__ == '__main__':
    main()
