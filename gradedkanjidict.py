﻿# -*- coding: utf-8 -*-

from kanjidict2 import KanjiDict2
from kanjilist import KanjiList
from kanjisubdict import KanjiSubDict

class GradedKanjiDict:
    """Extraction from the main dictionary basing on a kanji list"""
    def __init__(self):
        mainDictonary = KanjiDict2()
        kanjiList = KanjiList()

        self.dicts = {}
        self.dicts['jlpt5'] = KanjiSubDict(mainDictonary, kanjiList.getJlptList(4))
        self.dicts['jlpt4'] = KanjiSubDict(mainDictonary, kanjiList.getJlptList(3))
        self.dicts['jlpt3'] = KanjiSubDict(mainDictonary, kanjiList.getJlptList(2))
        self.dicts['jlpt2'] = KanjiSubDict(mainDictonary, kanjiList.getJlptList(1))
        self.dicts['jlpt1'] = KanjiSubDict(mainDictonary, kanjiList.getJlptList(0))

        self.dicts['joyo']    = KanjiSubDict(mainDictonary, kanjiList.getJoyoList())
        self.dicts['kyouiku'] = KanjiSubDict(mainDictonary, kanjiList.getKyouikuList())

    def getDict(self, name):
        return self.dicts[name]

    def hasDict(self, name):
        return name in self.dicts

def main():
    pass

if __name__ == '__main__':
    main()
