# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import re
sys.path.append(os.path.abspath('..'))
from kanjidict import  Kanjidict

class TestKanjidict(unittest.TestCase):
    def setUp(self):
      self.d = Kanjidict()

    def testList(self):
      self.assertEqual('哀', self.d.getList()[1].kanji)
      self.assertEqual({'アイ', 'あわ'}, self.d.getList()[1].readings)
      self.assertEqual({''}, self.d.getList()[0].tags)
      self.assertEqual({'ケン', 'コン'}, self.d.getList()[532].readings)

    def testKanjiTable(self):
      rows = 0
      table = list(self.d.getKanjiTable())
      self.assertEqual(2136, len(table))

    def testTags(self):
      tags = self.d.getAllTags()
      self.assertEqual(tags, {'', 'nature', 'goods', 'human', 'neg', 'body',
                              'mind', 'pos', 'color', 'space', 'area', 'act'})

    def testTagMap(self):
      tagMap = self.d.getTagMap()
      self.assertEqual(tagMap['color'], [14, 240])

    def testYomiMap(self):
      readings = self.d.getOnyomiMap()
      self.assertEqual(readings['アイ'], [2, 3, 4, 5])
      readings = self.d.getKunyomiMap()
      self.assertEqual(readings['あわ'], [2, 634, 1340, 1791, 1838])

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestKanjidict)
    unittest.TextTestRunner(verbosity=2).run(suite)
