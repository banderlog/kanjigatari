﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import itertools
import operator
sys.path.append(os.path.abspath('..'))
from freqdict import FreqDictionary
from jedict import JEDict

dictionary = FreqDictionary(None)
jedict = JEDict()
filteredDict = FreqDictionary(lambda x: jedict.contains(x))

class TestFreqDictionary(unittest.TestCase):
    def setUp(self):
        pass

    def testFreq(self):
        it = dictionary.getWordsByFrequency()
        l = list(it)
       # print(len(l))
        mostFreq10Words = l[0:10]
        self.assertEqual(['人', '日', '的', '者', '情報', '年', '中', '方', '私', '一'], mostFreq10Words)

    def testContaining(self):
        words = list(dictionary.getWordsContaining('日'))
        #print(len(words))
        self.assertEqual(1094, len(words))

    def testMissing(self):
        words = list(dictionary.getWordsContaining('尬'))
        self.assertEqual([], words)

        kanjiList = filteredDict.getMostRelatedKanji('珂')
        self.assertEqual([], kanjiList)
        wordList  = filteredDict.getWordsContainingWithFreq('珂')
        self.assertEqual([], wordList)

    def testRelatedKanji(self):
        kanjiList = list(dictionary.getMostRelatedKanji('日', 2))
        kanji = list(map(operator.itemgetter(0), kanjiList))
        weights = list(map(operator.itemgetter(1), kanjiList))
        succeeding = list(map(operator.itemgetter(2), kanjiList))
        self.assertEqual(['本', '今', '記', '昨', '明', '毎', '曜'], kanji)
        self.assertEqual([31, 14, 5, 5, 5, 4, 4], weights)
        self.assertEqual([True, False, True, False, False, False, False], succeeding)

    def testRelatedKanjiWithLargerThreshold(self):
        kanjiList = list(dictionary.getMostRelatedKanji('英', 5))
        kanji = list(map(operator.itemgetter(0), kanjiList))
        succeeding = list(map(operator.itemgetter(2), kanjiList))
        self.assertEqual(['語', '国', '会'], kanji)
        self.assertEqual([True, True, True], succeeding)

    def testRelatedKanjiFiltered(self):
        kanjiList = filteredDict.getMostRelatedKanji('英', 5)
        kanjiList = list(kanjiList)
        kanji = list(map(operator.itemgetter(0), kanjiList))
        succeeding = list(map(operator.itemgetter(2), kanjiList))
        self.assertEqual(['語', '国', '会', '雄'], kanji)
        self.assertEqual([True, True, True, True], succeeding)

    def testWordRelated(self):
        words = filteredDict.getWordsContainingWithFreq('雄')
        #print(str(list(words)))
        self.assertEqual(len(words), 34)
        self.assertEqual(words[0:3], [['英雄', 1], ['雄', 1], ['雄大', 2]])

    def testMultipleRequest(self):
        words = filteredDict.getWordsContainingWithFreq("亜")
        freqs = list(map(operator.itemgetter(1), words))
        #print(str(list(words)))
        #self.assertEqual(len(words), 34)
        self.assertEqual(freqs[0:5], [1, 4, 4, 11, 22])

        words = filteredDict.getWordsContainingWithFreq("亜")
        freqs = list(map(operator.itemgetter(1), words))
        self.assertEqual(freqs[0:5], [1, 4, 4, 11, 22])


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFreqDictionary)
    unittest.TextTestRunner(verbosity=2).run(suite)