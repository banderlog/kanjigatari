﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import operator
import itertools
sys.path.append(os.path.abspath('..'))
from kankendict import KankenDict

kd = KankenDict()

class TestKankenDict(unittest.TestCase):
    def setUp(self):
        pass

    def testInit(self):
        self.assertIn('竣', kd.getKanjiLevel1h() )
        self.assertIn('蘯', kd.getKanjiLevel1() )
        l = kd.getKanjiLevel1hWithId()
        self.assertEqual((2142, '乎'), l[5])

    def testWordNumber(self):
        self.assertEqual(891, len(kd.getKanjiLevel1h()))
        self.assertEqual(3328, len(kd.getKanjiLevel1()))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestKankenDict)
    unittest.TextTestRunner(verbosity=2).run(suite)
