﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import html.parser
sys.path.append(os.path.abspath('..'))

from charutils import hasKanji, isKanji, hasKana
from readingguess import assimilatedStartingVowels
from dictreadingguess import DictReadingGuess

guess = DictReadingGuess()

def getReading(word):
    return guess.getAllReadings(word)[-1][1]


class TestReadingGuessReal(unittest.TestCase):
    def setUp(self):
        pass

    def testOneKanjiReadingConversion(self):
        p = guess.getKanjiReadings('学')
        self.assertEqual(p, ['がく', 'まな', 'まなび'])

        p = guess.getKanjiReadings('王')
        self.assertEqual(p, ['おう', 'のう'])

        #p = guess.getKanjiReadings('聞')
        #self.assertEqual(p, ['おう', 'ひき'])

    def testSimpleReading(self):
        # unknown reading of 三
        res = getReading('口三味線')
        res = [res[0]] + res[2:]
        self.assertEqual([('口', 'くち'),  ('味', 'み'), ('線', 'せん')], res) #('三', 'じゃ', 'さん'),

        # kunyomi of 王
        res = getReading('尊王党')
        self.assertEqual([('尊', 'そん'), ('王', 'のう'), ('党', 'とう')], res)

        # ず - す penalty
        res = getReading('引摺り')
        self.assertEqual([('引', 'ひき'), ('摺', 'ず', 'す'), ('り', 'り')], res)

    def testVerbKunyomi(self):
        # reading from V-masu form
        res = getReading('聞苦しい')
        self.assertEqual([('聞', 'きき'), ('苦', 'ぐる'), ('し', 'し'), ('い', 'い')], res)

        res = getReading('一撮')
        self.assertEqual([('一', 'ひと'), ('撮', 'つまみ')], res)

        # reading form V-masu of ru-verb
        res = getReading('絶間無く')
        self.assertEqual([('絶', 'たえ'), ('間', 'ま'), ('無', 'な'), ('く', 'く')], res)

    def testStartingPenalty(self):
        # penalty for んぷ
        res = getReading('分布')
        self.assertEqual([('分', 'ぶん'), ('布', 'ぷ', 'ふ')], res)

        # starting ゃ penalty
        res = getReading('駄酒落')
        self.assertEqual([('駄', 'だ'), ('酒', 'じゃ', 'しゅ'), ('落', 'れ', 'お')], res)

        res = getReading('折戸')
        self.assertEqual([('折', 'おり'), ('戸', 'ど', 'と')], res)

        # starting っ penalty
        res = getReading('切符')
        self.assertEqual([('切', 'きっ', 'き'), ('符', 'ぷ', 'ふ')], res)

        # starting char penalty
        res = getReading('お父様')
        self.assertEqual([('お', 'お'), ('父', 'とう', 'ふ'), ('様', 'さま')], res)

        # attach る to the 2nd but not the 3rd syllable
        res = getReading('火達磨')
        self.assertEqual([('火', 'ひ'), ('達', 'だる', 'だ'), ('磨', 'ま')], res)

        res = getReading('生毛')
        self.assertEqual([('生', 'うぶ', 'う'), ('毛', 'げ', 'け')], res)

        # 益 represents u-verb
        res = getReading('益益')
        self.assertEqual([('益', 'ます', 'ま'), ('益', 'ます', 'ま')], res)

    def testNonKataSymbols(self):
        pass
        #res = getReading('地価ＬＯＯＫれぽーと') # ちかるっくれぽーと
        #self.assertEqual([('益', 'ます', 'ま'), ('益', 'ます', 'ま')], res)

        #res = getReading('Ｇｏｏｇｌｅ八分')   # ぐーぐるはちぶ
        #self.assertEqual([('益', 'ます', 'ま'), ('益', 'ます', 'ま')], res)

        # ＯＦＦ会

    def testWebFormat(self):
        res = guess.getWebKanjiReading('動物学者', '物')
        self.assertEqual([{  'word': 'どうぶつがくしゃ',
                             'positions': [{'to': 4, 'from': 2, 'reading': 'ぶつ', 'onyomi': True}], 'word': 'どうぶつがくしゃ'}
                         ], res)

        res = guess.getWebKanjiReading('高等学校', '学')
        self.assertEqual([{  'word': 'こうとうがっこう',
                             'positions': [{'to': 6, 'from': 4, 'reading': 'がく', 'onyomi': True}], 'word': 'こうとうがっこう'}
                         ], res)

        res = guess.getWebKanjiReading('高等学校', 'ガ')
        self.assertEqual([{'positions': [], 'word': 'こうとうがっこう'}], res)

    def testWebMultipleReading(self):
        res = guess.getWebKanjiReading('明朝', '明')
        self.assertEqual([{'positions': [{'from': 0, 'to': 3, 'onyomi': True, 'reading': 'みょう'}],
                           'word': 'みょうちょう'},
                          {'positions': [{'from': 0, 'to': 2, 'onyomi': True, 'reading': 'みん'}],
                           'word': 'みんちょう'} ], res)

    def testWebMultipleKanjiOccurence(self):
       res = guess.getWebKanjiReading('目的的', '的')
       self.assertEqual([{'positions': [{'from': 2, 'to': 4, 'onyomi': True, 'reading': 'てき'},
                                        {'from': 4, 'to': 6, 'onyomi': True, 'reading': 'てき'}],
                           'word': 'もくてきてき'} ], res)

    def testArchaicReadings(self):
       res = guess.getWebKanjiReading('韓国', '韓')
       self.assertEqual([{'positions': [{'from': 0, 'to': 2, 'onyomi': True, 'reading': 'かん'}],
                           'word': 'かんこく'} ], res)

    def testOnlyArchaicReading(self):
        res = guess.getAllReadings('一層')
        self.assertEqual([('いっそう', [('一', 'いっ', 'いち'), ('層', 'そう')])], res)

    def testWebMultipleKanjiOccurence(self):
       res = guess.getWebKanjiReading('国々', '国')
       self.assertEqual([{'positions': [{'from': 0, 'to': 2, 'onyomi': False, 'reading': 'くに'},
                                        {'from': 2, 'to': 4, 'onyomi': False, 'reading': 'ぐに'}],
                           'word': 'くにぐに'} ], res)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestReadingGuessReal)
    unittest.TextTestRunner(verbosity=2).run(suite)