﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import operator
import itertools
sys.path.append(os.path.abspath('..'))
from jedict import JEDict

jd = JEDict()

class TestJEDict(unittest.TestCase):
    def setUp(self):
        pass

    def testInit(self):
        self.assertTrue( jd.contains('宇宙') )

    def testWordNumber(self):
        self.assertEqual(155452, len(jd.words))

    def testDefinition(self):
        pronunAndDefs = jd.getPronunciationAndDefs('吉兆')
        tags = jd.getTagsFromDefinition(pronunAndDefs[0][1])
        self.assertEqual({'n' , 'adj-no'}, tags)

    def testGikunOrAteji(self):
        pronunAndDefs = jd.getPronunciationAndDefs('田舎')
        res = jd.isGikunOrAteji(pronunAndDefs[0][1])
        self.assertTrue(res)

        pronunAndDefs = jd.getPronunciationAndDefs('煙草')
        res = jd.isIrregularUse(pronunAndDefs[0][1])
        self.assertTrue(res)

        pronunAndDefs = jd.getPronunciationAndDefs('鉛筆')
        res = jd.isGikunOrAteji(pronunAndDefs[0][1])
        self.assertFalse(res)

    def testMultipleReadings(self):
        pronunAndDefs = jd.getPronunciationAndDefs('明朝')
        self.assertEqual(len(pronunAndDefs), 2)
        self.assertEqual(pronunAndDefs[0], ('みょうちょう', '(n-adv,n-t) tomorrow morning/(P)'))
        self.assertEqual(pronunAndDefs[1][0], 'みんちょう')

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestJEDict)
    unittest.TextTestRunner(verbosity=2).run(suite)
