﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import operator
import itertools
sys.path.append(os.path.abspath('..'))
from freqdict import FreqDictionaryInPlace

textDict1 = \
"""
一両日\t10
鍋己\t30
両\t20
ナー\t3
"""

textDict2 = \
"""
一両な日\t10
両な己日\t30
な鍋己\t30
ナー\t3
"""

class TestFreqDictionaryBase(unittest.TestCase):
    def setUp(self):
        pass

    def testInit(self):
        fdInPlace = FreqDictionaryInPlace(textDict1, None)
        it = fdInPlace.getWordsByFrequency()
        words = list(it)
        self.assertEqual(['鍋己', '両', '一両日'], words)

    def testRelatedKanji1(self):
        fdInPlace = FreqDictionaryInPlace(textDict2, None)
        kanjiWithFreq = list(fdInPlace.getRelatedKanji('両'))
        self.assertEqual([['己', 41, True], ['日', 34, True], ['一', 27, False]], kanjiWithFreq)

        kanjiWithFreq = list(fdInPlace.getMostRelatedKanji('両', 28))
        kanjiWithFreq = list(kanjiWithFreq)
        self.assertEqual([ ['日', 34, True]], kanjiWithFreq[1:])
        self.assertEqual( ['己', True], kanjiWithFreq[0][0, 2])
        self.assertAlmostEqual( 41, kanjiWithFreq[0][1])


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFreqDictionaryBase)
    unittest.TextTestRunner(verbosity=2).run(suite)
