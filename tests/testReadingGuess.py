﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import html.parser
sys.path.append(os.path.abspath('..'))

from readingguess import ReadingGuess
from readingguess import estimateSimilarity, getCostForKanji

class TestReadingGuess(ReadingGuess):
    def __init__(self, words, kanji):
        super().__init__(lambda w : words[w], lambda k : kanji[k])

    def getReading(self, word):
        r = self.getAllReadings(word)
        if len(r) > 1:
            raise RuntimeError('Incorrect reaing number')
        return r[0][1] if len(r) == 1 else None

class TestUtils(unittest.TestCase):
    def setUp(self):
        pass

    def testOneKanjiReading(self):
        words = { '日本' : ['にほん'] }
        kanji = { '日' : ['に'] ,  '本' : ['ほん']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('日本')
        self.assertEqual(res, [('日', 'に'), ('本', 'ほん')])

    def testFewKanjiReadings(self):
        words = { '日本' : ['にほん'] }
        kanji = { '日' : ['に', 'にち'] ,  '本' : ['ほん', 'もと']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('日本')
        self.assertEqual(res, [('日', 'に'), ('本', 'ほん')])

    def testSkipKanjiReadings(self):

        words = { '可愛い' : ['かわいい'] }
        kanji = { '可' : ['か', 'コク'] ,  '愛' : ['あい', 'いと']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('可愛い')
        self.assertEqual(res, [('可', 'か'), ('愛', 'わい', 'あい'), ('い','い')])

    def testAssimilationKuK_kK(self):
        words = { '学校' : ['がっこう'] }
        kanji = { '学' : ['がく', 'まな'] ,  '校' : ['こう', 'きゅう']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('学校')
        self.assertEqual(res, [('学', 'がっ', 'がく'), ('校', 'こう')])

    def testAssimilationTsu_Sokuon(self):
        words = { '八千' : ['はっせん'] }
        kanji = { '八' : ['はち', 'やつ'] ,  '千' : ['せん', 'ち']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('八千')
        self.assertEqual(res, [('八', 'はっ', 'はち'), ('千', 'せん')])

    def testMixedScript(self):
        words = { 'お礼' : ['おれい'] }
        kanji = { '礼' : ['れい', 'らい'] ,  '千' : ['せん', 'ち']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('お礼')
        self.assertEqual(res, [('お', 'お'), ('礼', 'れい')])

    def testJjj(self):
        words = { '外部' : ['がいぶ'] }
        kanji = { '外' : ['げ', 'そと', 'がい'] ,  '部' : ['ぶ', 'へ']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('外部')
        self.assertEqual(res, [('外', 'がい'), ('部', 'ぶ')])

    def testJjj2(self):
        words = { '大紫' : ['おおむらさき'] }
        kanji = { '大' : ['だい', 'たい', 'おお'] ,  '紫' : ['むらさき', 'し']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading('大紫')
        self.assertEqual(res, [('大', 'おお'), ('紫', 'むらさき')])

    def testFailed1(self):
        words = { '口拭き' : ['くちふき'] }
        kanji = { '口' : ['こう', 'く', 'くち'],  '拭' : ['しき', 'しょく', 'ぬぐ', 'ふ']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading(list(words.keys())[0])
        self.assertEqual(res, [('口', 'くち'), ('拭', 'ふ'), ('き', 'き')])

    def testMultiplePath(self):
        words = { '文字' : ['もじ'] }
        kanji = { '文' : ['ぶん', 'もん', 'ふみ', 'あや'],  '字' : ['じ', 'な', 'あざ', 'あざな']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading(list(words.keys())[0])
        self.assertEqual(res, [('文', 'も', 'もん'), ('字', 'じ')])

    def testWordLongerThanPronunciation(self):
        words = { '海鷂魚': 'えい'}
        kanji = { '海' : ['かい', 'うみ'], '鷂' : ['よう', 'はいたか'],
                  '魚':['ぎょ', 'うお', 'ざかな', 'さかな']}
        g = TestReadingGuess(words, kanji)
        res = g.getReading(list(words.keys())[0])
        self.assertEqual(None, res)

    def testEstimation1(self):
        cost = estimateSimilarity('きき', 'きき')
        self.assertEqual(cost, 0)

        cost = estimateSimilarity('はち', 'はっ')
        self.assertEqual(cost, 1)
        cost = estimateSimilarity('はつ', 'はっ')
        self.assertEqual(cost, 1)

        cost = estimateSimilarity('はつ', 'は')
        self.assertEqual(cost, 3)
        cost = estimateSimilarity('あい', 'わい')
        self.assertEqual(cost, 3)

        cost = estimateSimilarity('ぶん', 'も')
        self.assertEqual(cost, 6)
        cost = estimateSimilarity('もん', 'も')
        self.assertEqual(cost, 3)
        cost = estimateSimilarity('ふみ', 'も')
        self.assertEqual(cost, 6)
        cost = estimateSimilarity('き', 'きき')
        self.assertEqual(cost, 3)

        cost = estimateSimilarity('んぷ', 'ふ')
        self.assertEqual(cost, 15)

        cost = estimateSimilarity('ちじゃ', 'み')
        self.assertEqual(cost, 9)

        cost = estimateSimilarity('じゃ', 'ぞう')
        self.assertEqual(cost, 6)

        cost = estimateSimilarity('ちじゃ', 'ぞう')
        self.assertEqual(cost, 9)

        cost = estimateSimilarity('ず', 'す')
        self.assertEqual(cost, 1)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUtils)
    unittest.TextTestRunner(verbosity=2).run(suite)