﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import pprint
import operator
import html.parser
sys.path.append(os.path.abspath('..'))

from charutils import hasKanji, isKanji, hasKana
from readingguess import assimilatedStartingVowels
from dictreadingguess import DictReadingGuess

guess = DictReadingGuess()

def printDebugInfo(word):
    s = 'FAILED {}, \'{}\', kanji readings: {}'.format(
            word,
            guess.getPronunciation(word),
            dict([(k, guess.getKanjiReadings(k)) for k in filter(isKanji, word)]))
    print(s)

arrayTsu = []
arrayGi = []
mapGI = {}

def analyze(word, pronunciation, items, guess):
    readings = ''
    mixedWithKana = hasKana(word)
    anomaly = False
    for index, item in enumerate(items):
        if len(item) == 3:
            if item[1][-1] == 'っ':
                if (item[2][-1] not in { 'つ' , 'ち', 'く' }):
                    prev = items[index+1]
                    arrayTsu.append((item[0] + prev[0], item[1] + prev[-1]))
            elif (item[1][0], item[2][0]) in assimilatedStartingVowels:
                arrayGi.append((word, item))
                v = mapGI.get(item[0][0], 0)
                v += 1
                mapGI[item[0][0]] = v
            elif not guess.isIrregularUse(word):
                anomaly = True
    if anomaly:
        return
        print( '{}\t{}\t{}'.format(word, pronunciation, items))

    return
    for item in items:
        if len(item) == 3:
            if isKanji(item[0]):
                print('{} not kanji reading: {}={} {}'.format(word, item[0], item[2], item[1]))
                readings += ('O' if guess.isOnyomi(item[0], item[2]) else 'K')
            else:
                print('{} kana change: {} {}', word, item[0], item[1])

        if mixedWithKana and isKanji(item[0]) and guess.isOnyomi(item[0], item[-1]):
            print(word, ': onymi mixed with kana', item)

        if not mixedWithKana and not guess.isOnyomi(item[0], item[-1]):
            print(word, 'kunyomi with no kana', item)


class TestAnomaly(unittest.TestCase):
    def setUp(self):
        pass

    def testAllDictionaryReadings(self):
        return
        words = list(filter(lambda x: hasKanji(x), guess.getAllWords()))
        failed = 0
        succeeded = 0
        for index, word in enumerate(words):
            try:
                res = guess.getReading(word)
                print(word, index)
                if res is None:
                    failed += 1
                    printDebugInfo(word)
                else:
                    succeeded += 1
            except Exception as e:
                print('EXCEPTION: ' + str(e))
        print('Succeeded: {}, failed: {}'.format(succeeded, failed))

    def testAnalyzeYomi(self):

        print('')
        words = list(filter(lambda x: hasKanji(x), guess.getAllWords()))
        for index, word in enumerate(words[:50000]):
            try:
                res = guess.getReading(word)
                #print(word, index)
                if res is None:
                    printDebugInfo(word)
                else:
                    analyze(word, guess.getPronunciation(word), res, guess)
            except Exception as e:
                print('EXCEPTION: ' + str(e))
                #raise
        print('--TSU--')
        pprint.pprint(arrayTsu)
        print('--GI--')
        ranked = [sorted(mapGI.items(), key=operator.itemgetter(1), reverse=True)]
        pprint.pprint(ranked)
        #pprint.pprint(arrayGi)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAnomaly)
    unittest.TextTestRunner(verbosity=2).run(suite)