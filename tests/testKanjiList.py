﻿# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import operator
import itertools
sys.path.append(os.path.abspath('..'))
from kanjilist import KanjiList
from gradedkanjidict import GradedKanjiDict

kl = KanjiList()

class TestKanjiList(unittest.TestCase):
    def setUp(self):
        pass

    def testKanjiNumber(self):
        self.assertEqual(2136, len(kl.getJoyoList()) )
        self.assertEqual(1006, len(kl.getKyouikuList()) )

        self.assertEqual(80,   len(kl.getJlptList(4)))
        self.assertEqual(246,  len(kl.getJlptList(3)))
        self.assertEqual(613,  len(kl.getJlptList(2)))
        self.assertEqual(985,  len(kl.getJlptList(1)))
        self.assertEqual(2221, len(kl.getJlptList(0)))

    def testKanjiContained(self):
        self.assertTrue(set(kl.getKyouikuList()).issubset(set(kl.getJoyoList())))

        superSet = set(kl.getJoyoList())
        self.assertTrue(set(kl.getJlptList(4)).issubset(superSet))
        self.assertTrue(set(kl.getJlptList(3)).issubset(superSet))
        self.assertTrue(set(kl.getJlptList(2)).issubset(superSet))
        self.assertTrue(set(kl.getJlptList(1)).issubset(superSet))

    def testGradedDicts(self):
        d = GradedKanjiDict()
        self.assertNotEqual(None, d.getDict('jlpt1'))
        self.assertNotEqual(None, d.getDict('joyo'))
        self.assertTrue(d.hasDict('joyo'))

    def testDictSortingOrder(self):
        d = GradedKanjiDict()
        sd = d.getDict('jlpt1')

        table = sd.getKanjiTable('onyomi')
        self.assertEqual(table[0], (2569, '峠'))

        table = sd.getKanjiTable('kunyomi')
        self.assertEqual(table[0], (157, '不'))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestKanjiList)
    unittest.TextTestRunner(verbosity=2).run(suite)
