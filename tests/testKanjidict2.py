# -*- coding: utf-8 -*-
import os.path
import unittest
import sys
import pprint
import time
import logging
import datetime
import time
import re
sys.path.append(os.path.abspath('..'))
from kanjidict2 import KanjiDict2, splitReadings
from kanjisubdict import KanjiSubDict

class TestKanjidict2(unittest.TestCase):
    def setUp(self):
      self.d = KanjiDict2()

    def testList(self):
        k = self.d.getKanji('且')
        # 且|B1 G8 S5 N23 V17 H3485 DK2173 L2034 IN1926 E1091 P4-5-1 I0a5.15 Yqie3 Yju1|ショ ソ ショウ か.つ|あき かつ||moreover, also, furthermore
        self.assertEqual('且', k.kanji)
        self.assertEqual({'ショ', 'ソ', 'ショウ'}, k.onyomi)
        self.assertEqual({'か'}, k.kunyomi)
        self.assertEqual('moreover, also, furthermore', k.meaning)
        for k in self.d.kanjiList.values():
            self.assertEqual(len(k.kanji), 1, 'Extra symbols in ' + k.kanji)

    def testTotalKanjiNumber(self):
        table = list(self.d.kanjiList)
        self.assertEqual(12618, len(table))

    def testSubDictInit(self):
        kl = ['包', '伺', '報' ]
        sd = KanjiSubDict(self.d, kl)
        self.assertEqual(len(sd.kanjiList), 3)
        onyomi = sd.getOnyomiMap()
        kunyomi = sd.getKunyomiMap()
        self.assertEqual(onyomi, {'ホウ': [1049, 1868], 'シ': [348]} )
        self.assertEqual(kunyomi, {'くる': [1049], 'うかが': [348], 'つつ': [1049], 'むく': [1868]})

    def testDeducedReadings(self):
        _, _, masuKunyomi = splitReadings('ブン モン き.く き.こえる')
        self.assertEqual({'きき', 'きこえ', 'きこえる'}, masuKunyomi)
        k = self.d.getKanji('指')
        self.assertEqual({ 'さし'}, k.masuKunyomi)
        k = self.d.getKanji('聞')
        self.assertEqual({ 'きき', 'きこえ', 'きこえる'}, k.masuKunyomi)

    def testConvertedKunyomi(self):
        k = self.d.getKanji('王')
        self.assertEqual({ 'ノウ'}, k.kunyomi)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestKanjidict2)
    unittest.TextTestRunner(verbosity=2).run(suite)
