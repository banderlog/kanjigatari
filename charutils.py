
def isOnlyKatakana(text):
   for a in text:
      if ord(a) >= 0x30A0 and ord(a) <= 0x30FF:
         pass
      else:
         return False
   return True

def isOnlyHiragana(text):
   for a in text:
      if ord(a) >= 0x3040 and ord(a) <= 0x309F:
         pass
      else:
         return False
   return True

def isKanji(sym):
    return ord(sym) >= 0x4E00 and ord(sym) <= 0x9FFF # CJK Unified Ideographs

def isKana(sym):
    return ord(sym) >= 0x30A0 and ord(sym) <= 0x30FF or \
           ord(sym) >= 0x3040 and ord(sym) <= 0x309F

def hasKanji(text):
    if len(text) == 0:
        return False
    for sym in text:
        if isKanji(sym):
            return True
    return False

def hasKana(text):
    for sym in text:
        if isKana(sym):
            return True
    return False