﻿
import os
import logging
import logging.handlers
import os.path

def setupLogger():
    if True:
        thisAppBaseDir = os.path.abspath(os.path.dirname(__file__))
        logDir = os.path.join(thisAppBaseDir, 'logs')
    else:
        logDir= os.path.expanduser('~/logs')
    try:
        os.mkdir(logDir)
    except:
        pass
    logFile = os.path.join(logDir, 'kanjigatari.txt')
    handler = logging.handlers.RotatingFileHandler(logFile, "a",
                        encoding = "utf-8", maxBytes=1024*512, backupCount=20)
    formatter = logging.Formatter('%(asctime)s %(levelname)5s: %(thread)d [%(filename)14s:%(lineno)d] %(message)s')
    handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.INFO)

def main():
    pass

if __name__ == '__main__':
    main()
