﻿# -*- coding: utf-8 -*-

import os.path
import re
import itertools
import csv
import operator
#from charutils import isOnlyKatakana, isOnlyHiragana

class KankenDict:
    def __init__(self):
        self.kanjiListLevel1h = dict()
        self.kanjiListLevel1 = dict()
        fileName = os.path.join(os.path.dirname(__file__), 'Kanken levels(copy) - Sheet 1.csv')
        id = 1
        with open(fileName, 'r', encoding='utf8') as csvfile:
           reader = csv.reader(csvfile)
           next(reader, None) # skip headers
           for row in reader:
              groups = list(row)
              if len(groups) != 9:
                  RuntimeError('No match:' + line)
              _, kanji, numberKANJIDIC, HeisigRTK, \
              jis, kanken, strokes, kana, meaning = groups
              if kanken == '1.5':
                self.kanjiListLevel1h[kanji.strip()] = id
              elif kanken == '1':
                self.kanjiListLevel1[kanji.strip()] = id
              id += 1

    def getKanjiLevel1h(self):
        return self.kanjiListLevel1h.keys()

    def getKanjiLevel1hWithId(self):
        s = sorted(self.kanjiListLevel1h.items(), key=operator.itemgetter(1))
        return list(map(lambda x: (x[1], x[0]), s))

    def getKanjiLevel1(self):
        return self.kanjiListLevel1.keys()

    def getKanjiLevel1WithId(self):
        return list(sorted(self.kanjiListLevel1.items(), key=operator.itemgetter(1)))


def main():
    pass

if __name__ == '__main__':
    main()
