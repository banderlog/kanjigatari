﻿# -*- coding: utf-8 -*-

import operator
from jcconv_3x import kata2hira
from readingguess import ReadingGuess
from kanjidict2 import KanjiDict2
from jedict import JEDict

class DictReadingGuess(ReadingGuess):
    def __init__(self):
        self.kanjiDict = KanjiDict2()
        self.jdict = JEDict()
        super().__init__(lambda word : self.getRelevantPronunciations(word),
                         lambda kanji : self.getKanjiReadings(kanji))

    def getRelevantPronunciations(self, word):
        pronunAndDefs = list(self.jdict.getPronunciationAndDefs(word))
        filteredItems = list(filter( lambda d: not self.isArchaic(d[1]), pronunAndDefs))
        if len(filteredItems) == 0 and len(pronunAndDefs) != 0:
            filteredItems.append(pronunAndDefs[0])
        return map(operator.itemgetter(0), filteredItems)

    def getKanjiReadings(self, kanji):
        kanjiItem = self.kanjiDict.kanjiList[kanji]
        readings = [kata2hira(onyomi) for onyomi in kanjiItem.onyomi]
        kunyomi = (kata2hira(kunyomi) for kunyomi in kanjiItem.kunyomi)
        readings.extend(kunyomi)
        masuKunyomi = (kata2hira(kunyomi) for kunyomi in kanjiItem.masuKunyomi)
        readings.extend(masuKunyomi)
        return readings

    def getAllWords(self):
        return self.jdict.getAllWords()

    def getPronunciation(self, word):
        return self.jdict.getPronunciation(word)

    def isOnyomi(self, kanji, reading):
        kanjiItem = self.kanjiDict.kanjiList[kanji]
        return reading in set([kata2hira(onyomi) for onyomi in kanjiItem.onyomi])

    #def isIrregularUse(self, definition):
    #    return self.jdict.isIrregularUse(definition)

    def isArchaic(self, definition):
        return self.jdict.isArchaic(definition)

    def getWebKanjiReading(self, word, kanji):
        res = []

        for pronunciation, corr in self.getAllReadings(word):
            pos = 0
            out = { 'word' : '', 'positions' : [] }
            prevSymbol = None
            for item in corr:
                if kanji == item[0] or item[0] == '々' and kanji == prevSymbol:
                    occurence = {
                              'from' : pos,
                              'to':    pos + len(item[1]),
                              'reading' : item[-1],
                              'onyomi'  : self.isOnyomi(kanji, item[-1])
                               }
                    out['positions'].append(occurence)
                pos += len(item[1])
                out['word'] += item[1]
                if item[0] != '々':
                    prevSymbol = item[0]
            res.append(out)
        return res

def main():
    pass

if __name__ == '__main__':
    main()
