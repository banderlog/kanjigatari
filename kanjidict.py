# -*- coding: utf-8 -*-
# old   the official kyoujitai specified in the standard joyo table;
#       it does not include unofficial, extended, or Asahi characters.
#

import os.path
import re
import itertools
import csv
from charutils import isOnlyKatakana, isOnlyHiragana

class Kanji:
   def __init__(self, args):
      assert(len(args) == 10)
      self.id      = int(args[0])
      self.kanji   = args[1].strip()
      self.old     = args[2].strip()
      self.radical = args[3].strip()
      self.strokes = args[4].strip()
      self.grade   = args[5].strip()
      self.yearAdded = args[6].strip()
      self.meaning = args[7].strip()
      self.readings =  args[8]
      self.tags = args[9]

def splitReadings(text):
   for s in text.split('、'):
      hyphen = s.find('-')
      yield s[0 : (hyphen)] if hyphen != -1 else s

def stripParenthesis(text):
   return text.strip('（）')

def normalize(text):
   for t in splitReadings(text):
      yield stripParenthesis(t)



class Kanjidict:
   def __init__(self):
        self.kanjiList = []
        fileName = os.path.join(os.path.dirname(__file__), 'joyo tagged - Sheet1.csv')
        with open(fileName, 'r', encoding='utf8') as csvfile:
           reader = csv.reader(csvfile)
           next(reader, None) # skip headers
           for row in reader:
              groups = list(row)
              if len(groups) != 10:
                  RuntimeError('No match:' + line)
              groups[8] = set(normalize(groups[8]))
              groups[9] = set(s.strip() for s in groups[9].split(','))
              self.kanjiList.append(Kanji(groups))

   def getList(self):
      return self.kanjiList

   def getAllTags(self):
      tags = set()
      for k in self.kanjiList:
         tags |= set(k.tags)
      return tags

   def getTagMap(self):
      tags = {}
      for k in self.kanjiList:
         for kanjiTag in k.tags:
            if len(kanjiTag):
               tags.setdefault(kanjiTag, []).append(k.id)
      return tags

   def getOnyomiMap(self):
      readings = {}
      for k in self.kanjiList:
         for yomi in k.readings:
            if len(yomi) and isOnlyKatakana(yomi):
               readings.setdefault(yomi, []).append(k.id)
      return readings

   def getKunyomiMap(self):
      readings = {}
      for k in self.kanjiList:
         for yomi in k.readings:
            if len(yomi) and not isOnlyKatakana(yomi):
               readings.setdefault(yomi, []).append(k.id)
      return readings

   def getKanjiTable(self):
      a = ((item.id, item.kanji) for item in self.getList())
      return list(a)


if __name__ == '__main__':
   pass