﻿import os.path
import operator
import math
import logging
from charutils import isKanji, hasKanji
import copy

def normalizeWordFreq(wordAndFreq):
    wordAndFreq = copy.deepcopy(list(wordAndFreq))
    #print(wordAndFreq)
    if len(wordAndFreq) == 0:
        return wordAndFreq

    m = max(map(operator.itemgetter(1), wordAndFreq))
    for index in range(len(wordAndFreq)):
        wordAndFreq[index][1] = round(m / wordAndFreq[index][1])
    # print(wordAndFreq)
    return wordAndFreq

class BaseFreqDictionary:
    def __init__(self, lines, wordFilter):
        self.wordFilter = wordFilter if wordFilter else (lambda x: True)
        self.words = self.getWordPairs(lines)
        logging.info('Number of words in the dictionary: {}'.format(len(self.words)))
        self.kanjiToWord = {}

        for index in range(len(self.words)):
            for symbol in self.words[index][0]:
                if isKanji(symbol):
                    self.kanjiToWord.setdefault(symbol, set()).add(index)

    def getWordPairs(self, lines):
        words = []
        for line in lines:
            strippedLine = line.rstrip()
            if len(strippedLine) == 0:
                continue
            items = strippedLine.split('\t')
            if len(items) != 2:
                  raise RuntimeError(strippedLine, ': 3 or more components in the line' )
            word = items[0].strip()
            if hasKanji(word) and self.wordFilter(word):
                words.append([word, float(items[1])])

        allWordNumber = sum(map(operator.itemgetter(1), words))
        for i in range(len(words)):
            words[i][1] /= allWordNumber

        return words

    def getWordsByFrequency(self):
        sortedWords = sorted(self.words, key=operator.itemgetter(1), reverse=True)
        return map(operator.itemgetter(0), sortedWords)

    def getWordsContainingWithUnnormalizedFreq(self, syllable):
        assert(len(syllable) == 1)
        indexes = self.kanjiToWord.get(syllable)
        if indexes is None:
            return []

        #print(indexes)
        return (self.words[index] for index in indexes)

    def getWordsContainingWithFreq(self, syllable):
        words = self.getWordsContainingWithUnnormalizedFreq(syllable)
        words = normalizeWordFreq(words)
        return sorted(words, key=operator.itemgetter(1))

    def getWordsContaining(self, syllable):
        return map(operator.itemgetter(0), self.getWordsContainingWithUnnormalizedFreq(syllable))

    def __getRelatedKanjiWithAbsWeights(self, syllable):
        #relatedWordList = {}
        assert(len(syllable) == 1)
        relatedKanji = {}
        for word, freq in self.getWordsContainingWithUnnormalizedFreq(syllable):
           # print(word, freq)
            pos = word.find(syllable)
            if pos == -1:
                continue
            for index in range(0, len(word)):
                if index == pos or word[index] == syllable:
                    continue
                kanji = word[index]
                if isKanji(kanji):
                    #relatedWordList[word] = freq
                    absWeight = 1 / (2 ** (abs(pos - index) - 1))
                    item = relatedKanji.get(kanji, [0, 0])
                    absProximity = item[0] + freq * absWeight
                    relWeight = -absWeight if pos > index else absWeight
                    relProximity = item[1] + freq * relWeight
                    relatedKanji[kanji] = [absProximity, relProximity]

        #normalizeWordFreq(relatedWordList)
        return relatedKanji

    def __normalizeAndFlatten(self, relatedKanji):
        flattenKanji = list(map(lambda x: [ x[0], x[1][0], x[1][1] ], relatedKanji.items()))
        totalAbsWeight = sum(map(operator.itemgetter(1), flattenKanji), 0)
        for index in range(len(flattenKanji)):
            percentWeight = flattenKanji[index][1] / totalAbsWeight
            doesSucceed = flattenKanji[index][2] >= 0
            flattenKanji[index] = [flattenKanji[index][0], percentWeight, doesSucceed]
        return flattenKanji

    def getRelatedKanji(self, syllable):
        """
        return pairs of (kanji, weight), where wheight is in [0, 100]
        """
        relatedKanji = self.__getRelatedKanjiWithAbsWeights(syllable)
        flattenKanji = self.__normalizeAndFlatten(relatedKanji)

        # sort in double for the stability sake
        flattenKanji = sorted(flattenKanji, key= operator.itemgetter(1), reverse=True)
        for index in range(len(flattenKanji)):
            flattenKanji[index][1] = math.ceil(flattenKanji[index][1] * 100)
        return flattenKanji

    def getMostRelatedKanji(self, kanji, threshold=1):
        """
           threshold in percents, minimal frequency limit that kanji must satisfy
        """
        kanjiList = self.getRelatedKanji(kanji)
        return list(filter(lambda x: x[1] > threshold, kanjiList))

class FreqDictionaryInPlace(BaseFreqDictionary):
    def __init__(self, text, wordFilter):
        lines = text.split('\n')
        super().__init__(lines, wordFilter)

class FreqDictionary(BaseFreqDictionary):
    def __init__(self, wordFilter):
        lines = self.getCorpus()
        super().__init__(lines, wordFilter)

    def getCorpus(self):
        fileName = os.path.join(os.path.dirname(__file__), '1gm-0000.txt')
        with open(fileName, 'r', encoding='utf-8') as f:
            return f.readlines()

def main():
    pass

if __name__ == '__main__':
    main()
