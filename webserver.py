﻿# -*- coding: utf-8 -*-
import traceback
import os.path
import logging
import itertools
from flask import Flask, render_template, jsonify, json, request, abort
from werkzeug.exceptions import BadRequest
#from kanjidict import Kanjidict
from gradedkanjidict import GradedKanjiDict
#from kankendict import KankenDict
from charutils import isKanji
from freqdict import FreqDictionary
from jedict import JEDict
from dictreadingguess import DictReadingGuess
from utils import setupLogger

app = Flask(__name__)
#kanjiDict = Kanjidict()
kanjiDicts = GradedKanjiDict()
#kankenDict = KankenDict()
jedict = JEDict()
freqDict = FreqDictionary(lambda x: jedict.contains(x))
guessReading = DictReadingGuess()

@app.route("/")
def startPage():
    try:
        level  = request.args.get('list', 'jlpt5')#'joyo')
        sortby = request.args.get('sortby', 'id')

        print('Level:', level)
        print('SortBy:', sortby)

        if not kanjiDicts.hasDict(level):
            level = 'joyo'
        kanjiDict = kanjiDicts.getDict(level)
        totalKanjiList = kanjiDict.getKanjiTable(sortby)
        page = render_template('kanji_list.html',
                     kanjiTable = totalKanjiList,
                     tagMap = json.dumps(kanjiDict.getTagMap()),
                     onyomiMap = json.dumps(kanjiDict.getOnyomiMap()),
                     kunyomiMap = json.dumps(kanjiDict.getKunyomiMap()
                     ))
        return page
    except:
        logging.exception('')
        raise

@app.route("/related_kanji", methods=['GET', 'POST'])
def relatedKanji():
    try:
        kanji = request.args.get('kanji', '')
        if len(kanji) != 1 or not isKanji(kanji):
            raise BadRequest(kanji + ' is not kanji')

        kanjiList = freqDict.getMostRelatedKanji(kanji)
        wordList  = freqDict.getWordsContainingWithFreq(kanji)

        #import operator
        #print(list(map(operator.itemgetter(1), wordList)))

        return jsonify({ 'kanji' : kanji,
                         'related_kanji' : kanjiList, 'word_list': wordList})
    except:
        logging.exception('')
        raise

@app.route("/get_kanji_reading")
def getKanjiReading():
    try:
        kanji = request.args.get('kanji', '')
        word =  request.args.get('word', '')

        if len(word) == 0:
            raise BadRequest('Word is empty')

        if len(kanji) != 1 or not isKanji(kanji):
            raise BadRequest(kanji + ' is not kanji')

        if word.find(kanji) == -1:
            raise BadRequest(kanji + ' no such kanji in ' + word)

        res = guessReading.getWebKanjiReading(word, kanji)

        return jsonify({ 'res' : res })
    except:
        logging.exception('')
        raise


def main():
   setupLogger()
   app.run(host='0.0.0.0', port=8000, debug=True, use_reloader=True)

if __name__ == '__main__':
    main()
