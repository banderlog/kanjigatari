﻿# -*- coding: utf-8 -*-

import operator
import pprint
import logging
from jcconv_3x import kata2hira
from charutils import isKanji
import copy

kaRow = 'かきくけこ'
gaRow = 'がぎぐげご'
zaRow = 'ざじずぜぞ'
daRow = 'だぢづでど'
baRow = 'ばびぶべぼ'
paRow = 'ぱぴぷぺぽ'
saRow = 'さしすせそ'
taRow = 'たちつてと'
haRow = 'はひふへほ'
kyaRow = 'きゃきゅきょ'
shaRow = 'しゃしゅしょ'
chaRow = 'ちゃちゅちょ'
hyaRow = 'ひゃひゅひょ'
gyaRow = 'ぎゃぎゅぎょ'
jaRow = 'じゃじゅじょ'
jaRow2 = 'ぢゃぢゅぢょ'
byaRow = 'びゃびゅびょ'
pyaRow = 'ぴゃぴゅぴょ'

def getRules(u, v):
    a = set( zip(u, v) )
    a |= (set( zip(v, u) ))
    return a

assimilatedStartingVowels = getRules(kaRow, gaRow) | getRules(saRow, zaRow) | getRules(taRow, daRow) | \
                    getRules(haRow, paRow) | getRules(haRow, baRow) | getRules(paRow, baRow) | \
                    getRules(kyaRow, gyaRow) | getRules(shaRow, jaRow) | getRules(chaRow, jaRow2) | \
                    getRules(hyaRow, pyaRow) | getRules(hyaRow, byaRow) | getRules(pyaRow, byaRow)

assimilatedEndingVowels = {
                    ('ち', 'か') : 'っ', ('ち', 'き') : 'っ', ('ち', 'く' ) : 'っ', ('ち', 'け' ) : 'っ', ('ち', 'こ') : 'っ',
                    ('ち', 'さ') : 'っ', ('ち', 'し') : 'っ', ('ち', 'す' ) : 'っ', ('ち', 'せ' ) : 'っ', ('ち', 'そ') : 'っ',
                    ('く', 'か') : 'っ', ('く', 'き') : 'っ', ('く', 'く' ) : 'っ', ('く', 'け' ) : 'っ', ('く', 'こ') : 'っ',
                     }

assimilatedEndingVowels2 = set( [ ('ち', 'っ'), ('つ', 'っ'), ('く', 'っ') ] )

costNoCorrespondence = 10
costIdentical = 0
costVoicing = 1
costAssimilation = 1
costLeadingChars = 1
costDiffSyllable = 3
costCantStartWord = costNoCorrespondence

def getPenaltyForStart(syllable):
    if syllable in { 'ん', 'ぁ', 'ぃ', 'ぅ', 'ぇ', 'ぉ', 'ゃ', 'ゅ', 'ょ', 'っ'}:
        return costCantStartWord
    else:
        return 0

def getPrefixCost(x, y):
    cost = 0
    for xSyllable, ySyllable in zip(x, y):
      if xSyllable == ySyllable:
         pass
      elif  (xSyllable , ySyllable) in assimilatedStartingVowels:
         cost += costAssimilation
      else:
         cost += costDiffSyllable
    return cost

def estimatePrefixSuffix(x, y):
    x, y = (x, y) if len(x) > len(y) else (y, x)
    diffSize = len(x) - len(y)
    diffPenalty = diffSize * costDiffSyllable
    startPenalty = getPenaltyForStart(x[0])

    prefixCost = getPrefixCost(x[:(diffSize + 1)], y)
    suffixCost = getPrefixCost(x[-diffSize::-1], y[::-1])
    prefixCost += diffSize * costDiffSyllable
    # penalize extra chars at the beginning
    suffixCost += diffSize * (costDiffSyllable + costLeadingChars)

    cost = min(prefixCost, suffixCost)
    cost += startPenalty
    return cost

def estimateSimilarity(kanjiReading, wordPart):
   cost = 0
   if len(kanjiReading) != len(wordPart):
      return estimatePrefixSuffix(kanjiReading, wordPart)

   if kanjiReading[0] != wordPart[0]:
      if  (kanjiReading[0] , wordPart[0]) in assimilatedStartingVowels:
         cost += costAssimilation
      else:
         cost += costDiffSyllable

   if len(kanjiReading) == 1:
        return cost

   if kanjiReading[-1] != wordPart[-1]:
      if  (kanjiReading[-1] , wordPart[-1]) in assimilatedEndingVowels2:
         cost += costAssimilation
      else:
         cost += costDiffSyllable

   for x, y in zip( kanjiReading[1:-1], wordPart[1:-1] ):
      if x != y:
         cost += costDiffSyllable

   return cost

def getCostForKanji(readings, wordPart):
    costs = [(reading, estimateSimilarity(reading, wordPart)) for reading in readings]
    m = min(costs, key=operator.itemgetter(1))
    return m[0], m[1]


class ReadingGuess:
    def __init__(self, wordDictionary, kanjiDictionary):
        """
            wordDictionary   object to get word readings: ['みんちょう', 'みょうちょう'] = wordDictionary('明朝')
            kanjiDictionary  object to get kanji readings: ['キョク'],  ['つぼね'] = kanjiDictionary('局')
        """
        self.wordDictionary = wordDictionary
        self.kanjiDictionary = kanjiDictionary

    def getAllReadings(self, word, verbose=False):
        wordReadings = self.wordDictionary(word)
       # if len(wordReadings) == 0:
       #     raise RuntimeError(wordReadings + ' word is not found in dictionary')

        readings = []
        for wordReading in wordReadings:
            corr = self.getCorrespondence(word, wordReading, verbose)
            if corr:
                readings.append((wordReading, corr))
        return readings

    def getCorrespondence(self, kanjiWord, pronunciation, verbose):
        logging.info(verbose)
        costMatrix = [ [ [ ] for _ in range(len(pronunciation) + 1)] for _ in range(len(kanjiWord) + 1) ]
        costMatrix[0][0] = [ (0, []) ]

        for kPos in range(len(kanjiWord)):    # for all syllables in the word
            symbol = kanjiWord[kPos]
            for posPronunStart in range(kPos, len(pronunciation)):   # for all syllables in the pronunciation in hiragana
                prevCalcRes = costMatrix[kPos][posPronunStart]
                if prevCalcRes is None:
                    continue
                for posPronunEnd in range(posPronunStart, len(pronunciation)):   # for all combinations starting with the symbol
                    pronunPart = pronunciation[posPronunStart : (posPronunEnd + 1)]
                    symbolReading, cost = self.getCost(symbol, pronunPart)         # for all kanji reading
                    for prevCost, prevPath in prevCalcRes:
                        newCost = cost + prevCost
                        newPath = copy.copy(prevPath)
                        if symbolReading != pronunPart:
                            newPath.append((symbol, pronunPart, symbolReading))
                        else:
                            newPath.append((symbol, symbolReading))

                        costMatrix[kPos + 1][posPronunEnd + 1].append( (newCost, newPath) )

        if verbose:
            pprint.pprint(costMatrix)
        if costMatrix[-1][-1] == []:
            return None
        m = min( costMatrix[-1][-1], key=operator.itemgetter(0) )  # of the all success paths get the one with the minimal cost
        return m[1]


    def getCost(self, s, r):
        if s == r:
            return r, costIdentical
        if len(s) == 0 or len(r) == 0:
            return r, costNoCorrespondence

        if isKanji(s):
            readings = self.kanjiDictionary(s)
            return getCostForKanji(readings, r)
        elif (s, r) in assimilatedStartingVowels:
            return r, costVoicing
        else:
            return r, costNoCorrespondence



def main():
    pass

if __name__ == '__main__':
    main()
