﻿
import os
import re
import operator

rx = re.compile('\s*\((.+?)\)',  re.S)

class JEDict:
    def __init__(self):
        self.words ={}
        fileName = os.path.join(os.path.dirname(__file__), 'jedict.txt')
        with open(fileName, 'r', encoding='utf8') as f:
            for line in f.readlines():
                line = line.strip()
                items = line.split('|||')
                if len(items) != 3:
                    raise RuntimeError(line)
                word, pronunciation, definition = items
                if len(word) == 0:
                    continue
                self.words.setdefault(word, []).append((pronunciation, definition))

    def contains(self, word):
      #  print('Testing', word)
        return word in self.words

    def getAllWords(self):
        return self.words.keys()

    def getPronunciationAndDefs(self, word):
        return self.words[word]

    #def getDefinitions(self, word):
    #    return self.words[word][1]

    def isGikunOrAteji(self, definition):
        tags = self.getTagsFromDefinition(definition)
        return 'gikun' in tags or 'ateji' in tags

    def isIrregularUse(self, definition):
        tags = self.getTagsFromDefinition(definition)
        irregTags = {'gikun', 'ateji', 'uk', 'obs', 'arch' }
        return len(irregTags & tags) != 0

    def isArchaic(self, definition):
        tags = self.getTagsFromDefinition(definition)
        return 'arch' in tags

    def getTagsFromDefinition(self, definition):
        m = rx.search(definition)
        if m is None:
            return set()
        tags = set(g.strip() for g in m.group(1).split(','))
        return tags
##
##    def getTags(self, word):
##        wordDef = self.getDefinition(word)
##        m = rx.search(wordDef)
##        if m is None:
##            return set()
##        tags = set(g.strip() for g in m.group(1).split(','))
##        return tags


def main():
    pass

if __name__ == '__main__':
    main()
